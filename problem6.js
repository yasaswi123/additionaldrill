//6. Write a function called `surnameWithS` which returns a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).

function surnameWithS(got) {
    try{
        const namesWithSurnameS = Object.entries(got).reduce((group,presentValue)=>{
            presentValue[1].map((presentValue1)=>{
                const groupNames = presentValue1.people
                groupNames.map(presentvalue2=>{
                    const splitting = presentvalue2['name'].split(' ')
                    if(splitting[1].includes('S')){
                        group.push(presentvalue2.name)
                    }
                })
            })
            return group
        },[])
        return namesWithSurnameS
    }
    catch(error){
        console.error(error)
    }
}

module.exports = surnameWithS