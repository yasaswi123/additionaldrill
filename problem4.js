//4.Write a function called `nameWithS` which returns a array of names of all the people in `got` variable whose name includes `s` or `S`.

function nameWithS(got) {
    try{
        const namesOfPeopleWithS = Object.entries(got).reduce((person,currentValue)=>{
            currentValue[1].map(currentValue1=>{
                const peopleName = currentValue1.people
                peopleName.map(currentValue2=>{
                    if(((currentValue2.name).includes('s'))||((currentValue2.name).includes('S'))){
                        person.push(currentValue2.name)
                    }
                })
            })
            return person
        },[])
        return namesOfPeopleWithS
    }
    catch(error){
        console.error(error)
    }
    
}

module.exports = nameWithS