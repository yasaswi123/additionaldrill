//5.Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`

function nameWithA(got) {
    try{
        const namesOfPeopleWithA = Object.entries(got).reduce((person,currentValue)=>{
            currentValue[1].map(currentValue1=>{
                const peopleName = currentValue1.people
                peopleName.map(currentValue2=>{
                    if((currentValue2.name).includes('A')||(currentValue2.name).includes('a')){
                        person.push(currentValue2.name)
                    }
                })
            })
            return person
        },[])
        return namesOfPeopleWithA
    }
    catch(error){
        console.error(error)
    }
}

module.exports = nameWithA