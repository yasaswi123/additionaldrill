//1. Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.

function countAllPeople(got){
    try{
        const totalCount = got.houses.reduce((count,currentValue)=>{
            count+=currentValue.people.length
            return count
        },0)
        return totalCount
    }
    catch(error){
        console.error(error)
    }
}

module.exports = countAllPeople
