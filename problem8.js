//8. Write a function called `peopleNameOfAllHouses` which returns an object with the key of the name of house and value will be all the people in the house in an array

function peopleNameOfAllHouses(got) {
    try{
        const names = Object.entries(got).reduce((object,currentValue)=>{
            currentValue[1].map((currentValue1)=>{
                const houseName=currentValue1.people
                const array2=houseName.reduce((array1,currentValue2)=>{
                    if(object[currentValue1.name]){
                        array1.push(currentValue2.name)
                    }
                    else{
                        object[currentValue1.name]=array1.push(currentValue2.name)
                    }
                    return array1
                },[])
                object[currentValue1.name]=array2
            })
            return object
        },{})
        const sorting = Object.entries(names).sort()
        const result = Object.fromEntries(sorting)
        return result
    }
    catch(error){
        console.error(error)
    }
}

module.exports = peopleNameOfAllHouses