//2. Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.

function peopleByHouses(got){
    try{
        const eachPeoplehouses = Object.entries(got).reduce((house,currentValue)=>{
            currentValue[1].reduce((community,currentValue1)=>{
                let group = currentValue1.name
                let groupOfPeople = currentValue1.people
                let groupLength = groupOfPeople.length
                house[group]=groupLength
            },{})
            return house
        },{})
        const arrange = Object.entries(eachPeoplehouses).sort()
        const totalPeople = Object.fromEntries(arrange)
        return totalPeople
    }
    catch(error){
        console.error(error)
    }
}

module.exports = peopleByHouses