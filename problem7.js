
function surnameWithA(got) {
    try{
        const namesWithSurnameA = Object.entries(got).reduce((group,presentValue)=>{
            presentValue[1].map((presentValue1)=>{
                const peopleNames = presentValue1.people
                peopleNames.map((presentvalue2)=>{
                    const splitted = presentvalue2['name'].split(' ')
                    if(splitted[1].includes('A')){
                        group.push(presentvalue2.name)
                    }
                })
            })
            return group
        },[])
        return namesWithSurnameA
    }
    catch(error){
        console.error(error)
    }
}

module.exports = surnameWithA