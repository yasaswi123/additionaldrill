//3.Write a function called `everyone` which returns a array of names of all the people in `got` variable.

function everyone(got){
    try{
        const allPeople = Object.entries(got).reduce((person,currentValue)=>{
            currentValue[1].map(currentValue1=>{
                const names = currentValue1.people
                names.map(currentValue2=>{
                    person.push(currentValue2.name)
                })
            })
            return person
        },[])
        return allPeople
    }
    catch(error){
        console.error(error)
    }
}
module.exports = everyone